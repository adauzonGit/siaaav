package com.siaaa;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.apache.commons.io.FileUtils;

public class mainLauncher {
	static int Puerto = 6001;
	Process siaaa;
	static String launcher = "Launcher.exe";
	static String servidor = "\\\\sev\\in\\SisFS\\SIAAAFOX\\SIAAA\\";
	static String servidorL = "\\\\sev\\in\\SisFS\\SIAAAFOX\\SIAAA\\";
	static String runtimesS = "\\\\sev\\in\\SisFS\\SIAAAFOX\\SIAAA\\RUNTIMES_VFP9";
	static String runtimesL = "C:\\Windows\\SysWOW64\\";
	static String localL = "C:\\app\\";


	public static void main(String args[]) {

		File n = new File("C:\\app\\");

		n.mkdir();
		if (System.getProperty("sun.arch.data.model").contains("32")) {
			launcher = "Launcher32.exe";

		}

		servidorL = servidorL + launcher;
		localL = localL + launcher;
	
	
		mainLauncher main = new mainLauncher();


		System.out.println(System.getProperty("user.dir"));
		if (System.getProperty("user.dir").equalsIgnoreCase("c:\\app")) {
			main.startServer();
		} else {

			File fi = new File(servidorL);
			File fo = new File(localL);
			System.out.println(servidorL);
			System.out.println(localL);
			if (!fo.exists()) {
				System.out.println("Copiando por que no existe");
				main.copyFile(fi, fo);

			}
			if (fi.lastModified() != fo.lastModified()) {
				System.out.println("Copiando");
				main.copyFile(fi, fo);

			}
			if (fi.lastModified() == fo.lastModified()) {
				ProcessBuilder pro1 = new ProcessBuilder(localL);
				pro1.directory(new File("c:\\app\\"));
				try {
					pro1.start();
					System.exit(0);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}

	
	}

	boolean isAlive = true;

	public void copyFolder(File srcDir, File destDir) {
		try {


			FileUtils.copyDirectory(srcDir, destDir);


		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}

	public void copyFile(File fi, File fo) {
		try {
			File verify = new File("c:\\app\\siaaaverify." + fi.lastModified());
			verify.createNewFile();

			FileUtils.copyFile(fi, fo);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void startServer() {
		String ip = "";
		try {
			InetAddress inetAddress = InetAddress.getLocalHost();
			ip = inetAddress.getHostAddress();
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String USERDOMAIN = System.getenv("USERDOMAIN");
		String USERNAME = System.getenv("USERNAME");
		String USERDNSDOMAIN = System.getenv("USERDNSDOMAIN");
		String COMPUTERNAME = System.getenv("COMPUTERNAME");
		
		List<String> usuarios = UserService.usuarios;
		List<String> computernames = UserService.computernames;
		JOptionPane msg = new JOptionPane("Mensaje de advertencia. Se cerrara en 5 segundos", JOptionPane.WARNING_MESSAGE);
		if (!USERDOMAIN.equals("SEV")) {
			String message = "Tu usuario no es usuario en dominio SEV";
			Hss.alertmessage(msg, message, 5000);
			System.exit(0);
		}

		if (!usuarios.contains(USERNAME.toUpperCase())) {
			String message = "Tu usuario no esta habilitado para abrir SIAAA";
			Hss.alertmessage(msg, message, 5000);
			System.exit(0);
		}

		if (!USERDNSDOMAIN.equals("IN.SEV.GOB.MX")) {
			String message = "Tu maquina no esta en dominio SEV";
			Hss.alertmessage(msg, message, 5000);
			System.exit(0);
		}
		if (!computernames.contains(COMPUTERNAME)) {
			String message = COMPUTERNAME + " Tu maquina no esta habilitada\npara abrir SIAAA";
			Hss.alertmessage(msg, message, 5000);
			System.exit(0);
		}
		System.out.println(ip);
		if (!ip.startsWith("172.30.")) {
			String message = COMPUTERNAME + " No uses desbloqueadores de internet para utilisar esta aplicacion";
			Hss.alertmessage(msg, message, 5000);
			System.exit(0);
		}
		File f = null;
		f = new File("\\\\sev\\in\\SisFS\\SIAAAFOX\\SIAAA\\siaaa.exe");
		copyFolder(new File("\\\\Sev\\in\\SisFS\\SIAAAFOX\\SIAAA\\DBFTEMP"), new File("c:\\DBFTEMP"));
		copyFolder(new File("\\\\Sev\\in\\SisFS\\SIAAAFOX\\SIAAA\\app\\new\\dll"), new File("c:\\app"));
		if (f.exists()) {
			Thread t1 = new Thread(new Runnable() {
				@Override
				public void run() {
					InputStreamReader is = null;
					ServerSocket ss;
					try {
						ss = new ServerSocket(Puerto);
						while (isAlive) {
							Socket ch = ss.accept();
							new Thread(new Runnable() {

								@Override
								public void run() {
									try {
										isAlive = sa(COMPUTERNAME, isAlive, ch);
									} catch (IOException e) {
										e.printStackTrace();
									}
								}
							}).start();
						}
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			});

			try {
				String local = "c:\\app\\";
				File siaaaOrigen = new File(servidor + "siaaa.exe");		
				File siaaaDestino = new File(local + "\\siaaa" + System.getenv("USERNAME") + ".exe");
				copyFile(siaaaOrigen, siaaaDestino);
				ProcessBuilder pro2 = new ProcessBuilder(siaaaDestino.getCanonicalPath());
				pro2.directory(new File("\\\\sev\\in\\SisFS\\SIAAAFOX\\SIAAA\\"));
				siaaa = pro2.start();
				t1.start();
				while (siaaa.isAlive()) {
					Thread.sleep(2000);
				}
				t1.interrupt();
				System.exit(0);
				// siaaa.destroy();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			System.exit(0);
		}

	}

	private boolean sa(String COMPUTERNAME, boolean isAlive, Socket ch) throws IOException, UnknownHostException {
		InputStreamReader is;
		PrintWriter out = new PrintWriter(ch.getOutputStream(), true);
		is = new InputStreamReader(ch.getInputStream());
		BufferedReader in = new BufferedReader(is);

		String line = in.readLine();
		System.out.println(line);

		String comando = line.split("\\s+")[0];
		switch (comando) {
		case "close":
			siaaa.destroy();
			isAlive = false;
			out.println("Aplicacion cerrada");
			break;

		case "alive":
			out.println("Corriendo en  Equipo : " + COMPUTERNAME + " IP : " + InetAddress.getLocalHost().getHostAddress() + " Usuario : " + System.getProperty("user.name"));
			break;
		case "message":

			JFrame jf = new JFrame();
			jf.setAlwaysOnTop(true);
			JOptionPane.showMessageDialog(jf, line.substring(line.indexOf("-m ") + 3), "Mensaje del administrador", JOptionPane.INFORMATION_MESSAGE);
			String recibido = JOptionPane.showInputDialog(jf, "", "Mensaje de respuesta", JOptionPane.INFORMATION_MESSAGE);
			if (recibido == null)
				recibido = "Recibido";
			if (recibido.trim().isEmpty())
				recibido = "Recibido";
			out.println("Recibido  Respuesta : " + recibido);
			break;
		case "info":
			// Hss.addUser("imesa");
			break;

		default:
			out.println("comando no reconocido");
			break;
		}
		out.close();
		in.close();
		is.close();
		ch.close();
		return isAlive;
	}

	

}
