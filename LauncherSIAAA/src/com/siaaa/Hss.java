package com.siaaa;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.util.Arrays;
import java.util.List;
import java.util.prefs.Preferences;

import javax.swing.JDialog;
import javax.swing.JOptionPane;

import org.apache.commons.io.FileUtils;

public class Hss {

	
	public static void verifid() {
		ServerSocket socket = null;
		try {
			socket = new ServerSocket(58569);
		} catch (IOException e) {
			JOptionPane msg = new JOptionPane("Mensaje de advertencia. Se cerrara en 5 segundos", JOptionPane.WARNING_MESSAGE);
			alertmessage(msg, "Solo una instancia del Software SIAAA \npuede ser ejecutada al mismo tiempo", 5000);
			System.exit(0);
		}
	}

	public static void alertmessage(JOptionPane msg, String message, int time) {
		final JDialog dlg = msg.createDialog("Advertencia (Este cuadro se cerrara en 5 s)");

		msg.setMessage(message);

		dlg.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					Thread.sleep(time);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				dlg.setVisible(false);
			}
		}).start();
		dlg.setVisible(true);
	}


	
}
