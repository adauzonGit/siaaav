package com.siaaa.manager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;



public class mainManager {

	
	static String host = "ALL";
	public static void main(String args[]) {
		
		Scanner sc = new Scanner(System.in);
		boolean isAlive = true;
		System.out.print("Digite \n");
		while (isAlive) {
			
			String line = sc.nextLine();
			
			if (line.contains("-h ")) {
				int index = line.indexOf("-h ");
				host = line.substring(index + 3);
				host = host.split("\\s+")[0];
			}

			String commando = line.split("\\s+")[0];

			switch (commando) {
			case "ls":
				System.out.print("Buscando\n");
				cliente("alive", "ALL");
				break;
			case "close":
				System.out.print("Cerrando");
				cliente("close", host);
				break;
			case "message":
				new Thread(new Runnable() {
					@Override
					public void run() {
						cliente(line, host);
					}
				}).start();
				System.out.println("Esperando mensajes de retorno");
				break;
			case "exit":
				isAlive = false;
				sc.close();
				break;
			case "cls":
				System.out.println("\r");
				break;
			default:
				System.out.println("Comando no disponible : " + commando + " \n");
				break;
			}

		}

	}

	public static void cliente(String mensaje, String host) {
		List<String> computernames = new ArrayList<>();
		computernames.add("DAAI-YPOZOS");
		computernames.add("DAAI-VGARRIDO2");
		computernames.add("DAAI-GUSCANGA");
		computernames.add("DAAI-EMOLINA");
		computernames.add("DAAI-VVIVANCO2");
		computernames.add("DAAI-ETORRES");
		computernames.add("DAAI-VLOPEZ");
		computernames.add("DAAI-TEM");
		computernames.add("DAAI-RPAREDE2");
		computernames.add("DAAI-ROGARCIA");
		computernames.add("DAAI-RJAIME");
		computernames.add("DAAI-RGUERRE");
		computernames.add("DAAI-PALVAREZ");
		computernames.add("DAAI-NEALARCON");
		computernames.add("DAAI-MSANCHEZ2");
		computernames.add("DAAI-MMORA");
		computernames.add("DAAI-LREYNA");
		computernames.add("DAAI-LRAMIREZ1");
		computernames.add("DAAI-LCHACON2");
		computernames.add("DAAI-JOSALAS");
		computernames.add("DAAI-JMORENO");
		computernames.add("DAAI-JGONZALE2");
		computernames.add("DAAI-JAPO1");
		computernames.add("DAAI-ISLOPEZC");
		computernames.add("DAAI-ANLOPEZ");
		computernames.add("DAAI-IMESA2");
		computernames.add("DAAI-HOLMOS");
		computernames.add("DAAI-EFERAT2");
		computernames.add("DAAI-DECHEVERRIA");
		computernames.add("DAAI-DECHEVERRI");
		computernames.add("DAAI-CRODRIGUEZ");
		computernames.add("DAAI-RRODRIGUE2");
		computernames.add("DAAI-ANGEHERNA");
		computernames.add("DAAI-ROLIV");
		computernames.add("DAAI-BARGUELLEZ");
		computernames.add("DAAI-CLOMAN");
		computernames.add("DAAI-AGUEVARA1");
		computernames.add("DAAI-AHERRERAM");
		computernames.add("DAAI-AHERRERAPE");
		computernames.add("DAAI-LGARIBAY2");
		computernames.add("DAAI-ANRODRI");
		computernames.add("DAAI-YSARMIENTO");
		computernames.add("DAAI-ADAUZON");
		computernames.add("DAAI-MRUBIO");
		computernames.add("DAAI-ROGARCIA");
		computernames.add("DAYAI-ADAUZON2");
		computernames.add("DAAI-BCARREON");
		computernames.add("DAYAI-ABEBARRADAS");
		computernames.add("DAAI-DCERDAR");
		if (!host.equals("ALL")) {
			if (!computernames.contains(host.toUpperCase())) {
				computernames = new ArrayList<>();
				System.out.println("Host no en la lista de permitadas");
			} else {
				computernames = Arrays.asList(host);
			}
		} else {
//			computernames = new ArrayList<>();
//			for (int i = 1; i < 255; i++) {
//				computernames.add("172.30.50." + i);
//				computernames.add("172.30.28." + i);
//				computernames.add("172.30.52." + i);
//			}
		}
		
		computernames = new ArrayList<>();
		for (int i = 1; i < 255; i++) {
			computernames.add("172.30.50." + i);
			computernames.add("172.30.28." + i);
			computernames.add("172.30.52." + i);
		}
		// computernames = new ArrayList<>();

		// computernames.add("172.30.50.197");
		List<Thread> lst = new ArrayList<>();
//		computernames.stream().forEach(a -> {
//			System.out.println(a);
//		});
		for (String ipServidor : computernames) {

			// System.out.println(ipServidor);
			int puerto = 6001;

			Thread t2 = new Thread(new Runnable() {

				@Override
				public void run() {
					InputStreamReader is = null;
					try {

						Socket cs = new Socket();
						cs.connect(new InetSocketAddress(ipServidor, puerto), 2000);

						PrintWriter out = new PrintWriter(cs.getOutputStream(), true);
						is = new InputStreamReader(cs.getInputStream());
						BufferedReader in = new BufferedReader(is);
						System.out.println("Enviando comando a " + ipServidor);
						out.println(mensaje);

						String recibir = in.readLine();
						System.out.println(ipServidor + " " + recibir);
						out.close();
						in.close();
						is.close();
						cs.close();
					} catch (IOException ex) {
						// System.out.println(ex.getMessage());
					} finally {
						try {
							if (is != null)
								is.close();
						} catch (IOException ex) {
							// System.out.println(ex.getMessage());
						}
					}
				}
			});
			lst.add(t2);
			t2.start();

		}
		lst.forEach(f -> {
			while (f.isAlive()) {

			}
		});
		System.out.println("Termine");

	}

}
